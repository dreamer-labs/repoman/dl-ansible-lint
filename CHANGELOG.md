# [1.1.0](https://gitlab.com/dreamer-labs/repoman/dl-ansible-lint/compare/v1.0.1...v1.1.0) (2021-03-05)


### Features

* add git package ([e267bad](https://gitlab.com/dreamer-labs/repoman/dl-ansible-lint/commit/e267bad))

## [1.0.1](https://gitlab.com/dreamer-labs/repoman/dl-ansible-lint/compare/v1.0.0...v1.0.1) (2020-12-11)


### Bug Fixes

* remove ansible-lint quiet option ([e55ea8a](https://gitlab.com/dreamer-labs/repoman/dl-ansible-lint/commit/e55ea8a))

# 1.0.0 (2020-12-11)


### Features

* add container scanning pipeline ([a8c7ab5](https://gitlab.com/dreamer-labs/repoman/dl-ansible-lint/commit/a8c7ab5))
* initial commit ([7c7d5a5](https://gitlab.com/dreamer-labs/repoman/dl-ansible-lint/commit/7c7d5a5))
