# Dl Ansible Lint

This container packages Ansible-Lint and yamllint in a docker container for use in included pipelines.

## Versions
Versions of ansible-lint and yamllint are pinned in requirements.yml. Changes to requirements.yml should
bump the version of this container.

